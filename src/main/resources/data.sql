insert into person (id,name,email,password,role,enabled,birthday,created,modified) 
values ('dc952d19ccfc4164b5eb0338d14a6619','Mark','mark@gmail.com','secret','USER',true,'1960-03-29','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137');
insert into person (id,name,email,password,role,enabled,birthday,created,modified) 
values ('02288a3b194e49ceb1803f27be5df457','Matt','matt@gmail.com','secret','USER',true,'1980-07-03','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137');
insert into person (id,name,email,password,role,enabled,birthday,created,modified) 
values ('4fe22e358d0e4e38b680eab91787f041','Mike','mike@gmail.com','secret','ADMIN',true,'1982-08-05','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137');
insert into person (id,name,email,password,role,enabled,birthday,created,modified) 
values ('84e6c4776dcc42369510c2692f129644','Andy','andy@gmail.com','secret','ADMIN',false,'1976-10-11','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137');
insert into person (id,name,email,password,role,enabled,birthday,created,modified) 
values ('03a0c396acee4f6cb52e3964c0274495','Admin','admin@gmail.com','admin','ADMIN',true,'1978-12-22','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137');

insert into todo (id,person_id, description,created,modified,completed) 
values ('8a8080a365481fb00165481fbca90000', '84e6c4776dcc42369510c2692f129644', 'Read a Book','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137',true);
insert into todo (id,person_id, description,created,modified,completed) 
values ('ebcf1850563c4de3b56813a52a95e930', '84e6c4776dcc42369510c2692f129644', 'Buy Movie Tickets','2018-08-17 09:50:10.126','2018-08-17 09:50:10.126',false);
insert into todo (id,person_id, description,created,modified,completed) 
values ('78269087206d472c894f3075031d8d6b', '84e6c4776dcc42369510c2692f129644', 'Clean my Room','2018-08-17 07:42:44.136','2018-08-17 07:42:44.137',false);

