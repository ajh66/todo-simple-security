package com.ajh.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class ToDoUserDetailsService implements UserDetailsService {
	private static Logger logger = LoggerFactory.getLogger(ToDoUserDetailsService.class);

	private PersonRepository personRepo;
	private BCryptPasswordEncoder passenc;

	public ToDoUserDetailsService(PersonRepository personRepo, BCryptPasswordEncoder passenc) {
		super();
		this.personRepo = personRepo;
		this.passenc = passenc;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("Try to load user " + username);
		try {
			final Person person = this.personRepo.findByEmailIgnoreCase(username);
			if (person != null) {
				logger.info("Succeed to load user " + username);
				if (person.isEnabled()) {
					logger.info("Account " + username + " is active.");
				} else {
					logger.warn("Account " + username + " is locked.");
				}
//				PasswordEncoder passenc = PasswordEncoderFactories.createDelegatingPasswordEncoder();
				return User.withUsername(person.getEmail())
							.accountLocked(!person.isEnabled())
							.password(passenc.encode(person.getPassword()))
							.roles(person.getRole()).build();
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		throw new UsernameNotFoundException(username);
	}

}
