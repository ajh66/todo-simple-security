package com.ajh.spring;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;

public class SecurityHelper {
	private Collection<? extends GrantedAuthority> authorities = Collections.emptyList();
	private Authentication authentication;

	public SecurityHelper(SecurityContext context) {
		authentication = context.getAuthentication();
		if (authentication != null) {
			authorities = authentication.getAuthorities();
		}
	}

	public String username() {
		return authentication == null ? "" : authentication.getName();
	}
}
