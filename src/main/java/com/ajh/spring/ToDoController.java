package com.ajh.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/todo")
public class ToDoController {
	@Autowired
	private ToDoRepository repo;

	@GetMapping("/{id}")
	public ResponseEntity<ToDo> getToDoById(@PathVariable("id") String id) {
		java.util.Optional<ToDo> todo = repo.findById(id);
		if (todo.isPresent()) {
			return new ResponseEntity<>(todo.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
