package com.ajh.spring.integration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@Configuration
@EnableIntegration
@ImportResource("META-INF/todo-integration.xml")
public class ToDoIntegrationViaXml {

}
