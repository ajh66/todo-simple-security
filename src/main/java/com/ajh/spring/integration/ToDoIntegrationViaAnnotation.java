package com.ajh.spring.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;

import com.ajh.spring.ToDo;

@Configuration
@EnableIntegration
public class ToDoIntegrationViaAnnotation {
	@Bean
	public MessageChannel inputChannel02() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel transformChannel02() {
		return new DirectChannel();
	}

	@Bean MessageChannel logChannel02() {
		return new DirectChannel();
	}

	@MessageEndpoint
	class SimpleFilter {
		@Filter(inputChannel="inputChannel02", outputChannel="transformChannel02")
		public boolean process(ToDo message) {
			return message.isCompleted();
		}
	}

	@MessageEndpoint
	class SimpleTransformer {
		@Transformer(inputChannel="transformChannel02", outputChannel="logChannel02")
		public String process(ToDo message) {
			return message.getDescription().toUpperCase();
		}
	}

	@MessageEndpoint
	class SimpleServiceActivator {
		Logger log = LoggerFactory.getLogger(SimpleServiceActivator.class);
		@ServiceActivator(inputChannel="logChannel02")
		public void process(String message) {
			log.info(message);
		}
	}
	
}
