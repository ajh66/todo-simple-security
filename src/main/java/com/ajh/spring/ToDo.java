package com.ajh.spring;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="TODO")
public class ToDo {
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid")
	private String id;

	@NotNull
	@NotBlank
	private String description;

	@Column(insertable=true, updatable=false)
	private LocalDateTime created;

	private LocalDateTime modified;

	private boolean completed;

	public ToDo() {}

	public ToDo(String description) {
		this.description = description;
	}

	public ToDo(String description, boolean completed) {
		this.description = description;
		this.completed = completed;
	}

	@Override
	public int hashCode() {
		return Objects.hash(completed, description);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ToDo)) {
			return false;
		}
		ToDo other = (ToDo) obj;
		return completed == other.completed && Objects.equals(description, other.description);
	}

	@PrePersist
	void onCreate() {
		this.setCreated(LocalDateTime.now());
		this.setModified(LocalDateTime.now());
	}

	@PreUpdate
	void onUpdate() {
		this.setModified(LocalDateTime.now());
	}

	public String getId() {
		return id;
	}
	public String getDescription() {
		return description;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public LocalDateTime getModified() {
		return modified;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
