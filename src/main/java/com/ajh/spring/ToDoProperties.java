package com.ajh.spring;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="todo.ws")
public class ToDoProperties {
	private String app = "/todo-api-ws";
	private String broker = "/todo";
	private String endpoint = "/stomp";
	public String getApp() {
		return app;
	}
	public String getBroker() {
		return broker;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setApp(String app) {
		this.app = app;
	}
	public void setBroker(String broker) {
		this.broker = broker;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

}
