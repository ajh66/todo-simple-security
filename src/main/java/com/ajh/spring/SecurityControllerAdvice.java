package com.ajh.spring;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class SecurityControllerAdvice {
	private static Logger logger = LoggerFactory.getLogger(SecurityControllerAdvice.class);

	private SecurityHelper securityHelper;

	@ModelAttribute // DOES NOT work as expected!
	public void addDefaultAttributes(HttpServletRequest req, Model model) {
		securityHelper = new SecurityHelper(SecurityContextHolder.getContext());

		logger.info("User name is " + securityHelper.username());
		req.setAttribute("username", securityHelper.username());
		model.addAttribute("username", securityHelper.username());
	}

}
