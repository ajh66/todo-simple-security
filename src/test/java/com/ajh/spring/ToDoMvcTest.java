package com.ajh.spring;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
/*
@WebMvcTest(ToDoController.class)
*/
@SpringBootTest
@AutoConfigureMockMvc
public class ToDoMvcTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private ToDoRepository repo;

	@Test
	@WithMockUser
	public void todoControllerTest() throws Exception {
		ToDo todo = new ToDo("Do homework", true);
		todo.setId("random-id");
		given(repo.findById("random-id"))
			.willReturn(java.util.Optional.ofNullable(todo));
		mvc.perform(get("/todo/random-id").accept(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isOk())
			.andExpect(content().string("{\"id\":\"random-id\",\"description\":\"Do homework\",\"created\":null,\"modified\":null,\"completed\":true}"));
	}
}
