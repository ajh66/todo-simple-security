Chapter 8 "Security with Spring Boot" from the book "Pro Spring Boot 2" Apress 2019

Note:
> Once enabling Spring security, CSRF protection is enabled accordingly, all HTTP POST requests are required to provide CSRF token.

Using:
* View engine Mustache
* H2 database
* JPA entity & repository

Features:
* Basic web security
* Customized login page
* Customized UserDetailsService
* WebSocket basic demo (from chapter 9.5 "WebSockets with Spring Boot")

Web socket demo
---
1. In browser access http://localhost:8080/wsclient
2. In terminal run curl command, e.g. `curl -XPOST -d '{"description":"Learn to play Guitar"}' -H "Content-Type:application/json" http://localhost:8080/api/toDos -u mark@gmail.com:secret -i`